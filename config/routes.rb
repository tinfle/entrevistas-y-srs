Rails.application.routes.draw do

  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  devise_for :users, controllers: { confirmations: 'confirmations' }
  root 'books#index'
  get '/books/indexadmin', to: 'books#indexadmin', as: 'books_indexadmin'
  get '/books/showadmin/:id', to: 'books#showadmin', as: 'books_showadmin'
  get '/profiles/select', to: 'profiles#select', as: 'profiles_select'
  get '/profiles/cambiar', to: 'profiles#cambiar', as: 'profiles_cambiar'
  get '/profiles/selectplan', to: 'profiles#selectplan', as: 'profiles_selectplan'
  get '/profiles/cambiarplan1', to: 'profiles#cambiarplan1', as: 'profiles_cambiarplan1'
  get '/profiles/cambiarplan2', to: 'profiles#cambiarplan2', as: 'profiles_cambiarplan2'
  get '/profiles/cambiartarjeta', to: 'profiles#cambiartarjeta', as: 'profiles_cambiartarjeta'
  resources :notices
  resources :favorites
  resources :markers
  resources :seelaters
  resources :books
  resources :profiles
  resources :welcome
  resources :books do
  	resources :capitulos
  end
  resources :commentaries
  resources :votes
  resources :authors
  get '/books/:book_id/capitulos/:id/borrar', to: 'capitulos#borrar', as: 'borrar_book_capitulo'

end
