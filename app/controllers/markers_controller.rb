class MarkersController < ApplicationController
    def new
      @marker = Marker.new
    end
    def create
      @marker = Marker.new(marker_params)
      if (current_user.profiles.find(session[:profile_id]).markers.where(book_id: @marker.book_id).first != nil)
        current_user.profiles.find(session[:profile_id]).markers.where(book_id: @marker.book_id).first.destroy
      end
      @marker.save
      redirect_to book_capitulo_path(@marker.book_id,@marker.capitulo_id)
    end
    def destroy
       @marker = Marker.find params[:id]
       link = @marker.capitulo_id
       @marker.destroy
      redirect_to book_capitulo_path(@marker.book_id,@marker.capitulo_id)
    end
    def marker_params
      params.require(:marker).permit(:capitulo_id,:profile_id,:book_id)
    end
  end
