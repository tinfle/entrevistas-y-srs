class CapitulosController < ApplicationController




	def new
		@book = Book.find params[:book_id]

		@capitulo = Capitulo.new
		@capitulo.book_id = params[:book_id]

		@capitulo.numero = @book.capitulos.size + 1

	end
	def create
		@book = Book.find params[:book_id]
  		@capitulo = Capitulo.new(params.require(:capitulo).permit!)
  		@capitulo.book_id = params[:book_id]
		@capitulo.numero = @book.capitulos.size + 1

  		if (@capitulo.save)
			redirect_to root_path

		end

	end
	def edit
		redirect_to root_path unless current_user.admin?

		@book = Book.find params[:book_id]
		@capitulo = Capitulo.find params[:id]


		

	end
	def borrar
		redirect_to root_path unless current_user.admin?

		@capitulo = Capitulo.find params[:id]
		@capitulo.destroy
			
		redirect_to books_path
		flash[:notice] = "Capitulo borrado exitosamente"
	end

	

	def update
		@capitulo = Capitulo.find params[:id]
		redirect_to root_path unless current_user.admin?
		if(@capitulo.update(params.require(:capitulo).permit!))
			redirect_to books_indexadmin_path
			flash[:notice] = "Cambios realizados con éxito"
		else
			flash.now[:alert] = @capitulo.errors.full_messages.first
			render :edit
		end
	end
	def index
		@perfil = Profile.find(session[:profile_id])
		@book = Book.find params[:book_id]
		@capitulos = @book.capitulos.where("? < outdate AND ? > indate", Time.now,Time.now)
	end
	def show
		@capitulo = Capitulo.find params[:id]
		if (@capitulo.indate > Time.now or @capitulo.outdate < Time.now)
			@capitulo = nil
		end
		@book = Book.find params[:book_id]
		if (Seelater.where(book_id:@book.id,profile_id:session[:profile_id]).all.count > 0)
			Seelater.where(book_id:@book.id,profile_id:session[:profile_id]).last.destroy
		end
		if(current_user.profiles.find(session[:profile_id]).markers.where(capitulo_id: @capitulo.id).size == 0)
			@marker = Marker.new(capitulo_id: @capitulo.id, profile_id: session[:profile_id], book_id: @capitulo.book_id)
		else
			@marker = current_user.profiles.find(session[:profile_id]).markers.where(capitulo_id: @capitulo.id).first
		end
	end

end
