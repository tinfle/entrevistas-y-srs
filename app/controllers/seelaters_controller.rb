class SeelatersController < ApplicationController
  def new
    @seelater = Seelater.new
  end
  def create
    @seelater = Seelater.new(seelater_params)

    if (@seelater.save)
      redirect_to book_path(@seelater.book_id)
    else
      redirect_to book_path(@seelater.book_id)
    end

  end
  def destroy
     @seelater = Seelater.find params[:id]
     link = @seelater.book_id
     @seelater.destroy
     redirect_to book_path(link)
  end

  def index
    @seelater = Seelater.all
  end

  private

  def seelater_params
    params.require(:seelater).permit(:book_id,:profile_id)
  end
end
