class ProfilesController < ApplicationController
  before_action :authenticate_user!

  def new
    if current_user.familiar
      if current_user.profiles.size > 3
        redirect_to profiles_path
      end
    else
      if current_user.profiles.size > 1
        redirect_to profiles_path
      end
    end 
    @profile=Profile.new
  end


  def create
		@profile = Profile.new(profile_params)
    @profile.user_id = current_user.id

    yaExiste = false;
    current_user.profiles.each do |p|
      if(p.name == @profile.name)
        yaExiste = true;
      end
    end

    if(current_user.profiles.size < 4)
      if(not yaExiste)
        
        if (@profile.save)
    			flash[:notice] = "El perfil se creo exitosamente"
          redirect_to profiles_path
    		else
    			flash.now[:alert] = 'Ocurrió un error'
          render :new
    		end
      else
        flash.now[:alert] = 'Ya existe ese perfil'
        render :new
      end
    else
      flash.now[:alert] = 'Ya tienes muchos perfiles'
      render :new
    end
	end
    def destroy
      @profile = Profile.find params[:id]
      if current_user.profiles.size > 1
        Commentary.where(profile_id:@profile.id).all.each do |a|
          a.destroy
        end
        @profile.destroy
        session[:profile_id] = nil
        redirect_to profiles_path
      end
    end

  	def profile_params
  		params.require(:profile).permit(:name,:kids,:picture)
  	end

    def show
  		@profile = Profile.find params[:id]
  	end

    def edit

       @profile = Profile.find params[:id]
    end

    def update

    @profile = Profile.find params[:id]
    if(@profile.update(params.require(:profile).permit!))
      redirect_to profiles_path
      flash[:notice] = "Cambios realizados con éxito"
    else
      flash.now[:alert] = 'Ya existe un perfil con ese nombre'
      render :edit
    end

    end

    def index
      if(session[:profile_id] == nil)
        redirect_to profiles_select_path
      else
        @profiles = current_user.profiles
      end
    end

    def select
      if current_user.profiles.size == 0
        redirect_to new_profile_path
      else
        @profiles = current_user.profiles
      end
    end
    def cambiar
      session[:profile_id] = params[:perfil].to_i
      if(session[:profile_id] == nil)
        sign_out current_user
      else
        flash[:notice] = "Seleccionaste perfil correctamente"
      end
      redirect_to root_path
    end
    def cambiarplan1

      if(current_user.familiar == false)
        redirect_to profiles_selectplan_path 
        flash[:alert] = "Usted ya posee este Plan de suscripcion, Seleccione uno Nuevo!!"
      else

       if(current_user.profiles.count > 2)
         flash[:alert] = "Usted no cumple las condiciones para cambiarse al nuevo plan, el mismo requiere como maximo 2 perfiles, usted posee muchos, por favor elimine algunos antes de poder continuar"
         redirect_to profiles_path
       else
         @u=User.find(current_user.id)
         @u.familiar = false
         @u.save
         redirect_to profiles_path
         flash[:notice] = "Felicitaciones ha cambiado su plan de suscripcion, Su nuevo Plan es ahora ESTANDAR"
        end
      end
    end
    def cambiarplan2
      if(current_user.familiar == true)
        redirect_to profiles_selectplan_path 
        flash[:alert] = "Usted ya posee este Plan de suscripcion, Seleccione uno Nuevo!!"
      else
        @u=User.find(current_user.id)
        @u.familiar = true
        @u.save
        redirect_to profiles_path
        flash[:notice] = "Felicitaciones ha cambiado su plan de suscripcion, Su nuevo Plan es ahora FAMILIAR"
      end
    end
    def selectplan
      @profile = session[:profile_id]
    end

    def cambiartarjeta
      @user = current_user
      @user.card = nil
      @user.cvv = nil
    end
end
