class VotesController < ApplicationController

	def new
	end

	def create

		@punto = Vote.new(vote_params)

		if(@punto.save)
			@lib = Book.where(isbn:@punto.isbn).last
			if(Vote.where(isbn:@punto.isbn,profile_id:@punto.profile_id).all.count == 1)
			@lib.scores = @lib.scores + 1
			@lib.total = @lib.total + @punto.punctuation
			@lib.punctuation = @lib.total/@lib.scores
			else
			@lib.total = @lib.total - Vote.where(isbn:@punto.isbn,profile_id:@punto.profile_id).first.punctuation
			@lib.total = @lib.total + @punto.punctuation
			Vote.where(isbn:@punto.isbn,profile_id:@punto.profile_id).first.destroy
			end
			@lib.punctuation = @lib.total/@lib.scores
			@lib.save
			redirect_to book_path @lib
			flash[:notice] = "Tu puntuacion ha sido agregada exitosamente"
		else
			flash.now[:alert] = "Tu puntuacion no pudo ser guardada"
			render :new
		end
	end
	def vote_params
		params.require(:vote).permit(:isbn,:profile_id,:punctuation)
	end
end


