class AuthorsController < ApplicationController

	def create
		redirect_to root_path unless current_user.admin?

		@autor = Author.new(authors_params)

		if (@autor.save)
			redirect_to new_book_path

		else
			flash.now[:alert] = 'Problemas al agregar el nuevo autor'
			redirect_to new_book_path
		end
	end

	def edit
		redirect_to root_path unless current_user.admin?
		@a=params[:autor]
		@autor = Author.create(autor:@a)

		if (@autor.save)
			redirect_to edit_book_path

		else
			flash.now[:alert] = 'Problemas al agregar el nuevo autor'
			redirect_to edit_book_path
		end
	end

	private

	def authors_params
		params.require(:author).permit(:autor)
	end

end


