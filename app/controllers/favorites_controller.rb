class FavoritesController < ApplicationController
  def new
    @favorite = Favorite.new
  end
  def create

    @favorite = Favorite.new(favorite_params)

    if (@favorite.save)
      redirect_to book_path(@favorite.book_id)
    else
      redirect_to book_path(@favorite.book_id)
    end

  end
  def destroy
     @favorite = Favorite.find params[:id]
     link = @favorite.book_id
     @favorite.destroy
     redirect_to book_path(link)
  end

  def index
    @favorite = Favorite.all
  end

  private

  def favorite_params
    params.require(:favorite).permit(:book_id,:profile_id)

  end
end
