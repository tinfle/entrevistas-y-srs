class CommentariesController < ApplicationController


	def new
	end

	def create

		@comentario = Commentary.new(commentary_params)

		if(@comentario.save)
			redirect_to book_path Book.find(@comentario.book_id)
			flash[:notice] = "Tu comentario se guardo exitosamente"
		else
			flash.now[:alert] = "Tu comentario no pudo ser guardado"
			render :new
		end
	end


	def destroy
		

		@comentario = Commentary.find params[:id]
		@comentario.destroy
		redirect_to book_path Book.find(@comentario.book_id)
		flash[:notice] = "El comentario fue eliminado correctamente"
	end


	def commentary_params
		params.require(:commentary).permit(:book_id,:profile_id,:comment)
	end
end


