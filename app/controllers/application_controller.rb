class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :configure_permitted_parameters, if: :devise_controller?





  protected
  def authenticate_profile
    if(session[:profile_id] == nil)
      redirect_to profiles_select_path
    end
  end
  def configure_permitted_parameters
  	devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:surname,:email,:password,:date_of_birth,:address,:card,:cvv,:vencimiento,:familiar])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name,:surname,:email,:password,:password_confirmation,:date_of_birth,:address,:card,:cvv,:vencimiento,:familiar])
	end



end
