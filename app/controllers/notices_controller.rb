class NoticesController < ApplicationController

	respond_to :html, :json
  	before_action :authenticate_user!

  	def new
      redirect_to root_path unless current_user.admin?
  		@notice=Notice.new
  	end

  	def create
      redirect_to root_path unless current_user.admin?
  		@notice = Notice.new(params.require(:notice).permit!)

  		if (@notice.save)
  			redirect_to notices_path
  		end
  	end

  	def index
  		@notice = Notice.all
  	end

  	def show
  		@notice = Notice.find params[:id]
  	end
  	
  	def edit
  		 redirect_to root_path unless current_user.admin?
  	   @notice = Notice.find params[:id]
  	end

  	def destroy
      redirect_to root_path unless current_user.admin?
  		@notice = Notice.find params[:id]
  		@notice.destroy
  		redirect_to notices_path
  	end

  	def update

      redirect_to root_path unless current_user.admin?
      @notice = Notice.find params[:id]

    @notice = Notice.find params[:id]
    if(@notice.update(params.require(:notice).permit!))
      redirect_to books_indexadmin_path
      flash[:notice] = "Cambios realizados con éxito"
    else
      flash.now[:alert] = 'Ya existe una noticia con ese Titulo'
      render :edit
    end
  	end

  	private

	def notice_params
		params.require(:notice).permit(:titulo,:cuerpo,:picture)
	end

end
