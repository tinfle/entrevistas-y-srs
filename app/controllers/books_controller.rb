class BooksController < ApplicationController

	before_action :authenticate_user!
	before_action :authenticate_profile


	def new
		redirect_to root_path unless current_user.admin?

		@book=Book.new
		@autor = Author.new

	end

	def create
		redirect_to root_path unless current_user.admin?

		@book = Book.new(book_params)
		@book.punctuation=0
		@book.scores=0
		@book.total=0

		if (@book.save)
			redirect_to @book
			flash[:notice] = "El libro se guardó con éxito"
		else
			flash.now[:alert] =  @book.errors.full_messages.first
			render :new
		end

	end

	def index
		if !(Profile.find(session[:profile_id]).kids)
			if @b = params[:buscador]
				@books = Book.where('titulo LIKE ? AND  outdate > ? AND indate < ?', "%#{@b}%", Time.now , Time.now)
			else
				#@books = Book.all
				@books = Book.where("? < outdate AND ? > indate", Time.now,Time.now)
			end
		else
			if @b = params[:buscador]
				@books = Book.where('titulo LIKE ? AND  outdate > ? AND indate < ?', "%#{@b}%", Time.now , Time.now).where(kids: true)
			else
				#@books = Book.all
				@books = Book.where("? < outdate AND ? > indate", Time.now, Time.now).where(kids: true)
			end
		end
	end
	def indexadmin
		redirect_to root_path unless current_user.admin?
	end
	def showadmin
		redirect_to root_path unless current_user.admin?
		@book = Book.find params[:id]
	end


	def show
		@book = Book.find params[:id]
		if (@book.indate > Time.now or @book.outdate < Time.now)
			@book = nil
		else
			if (Profile.find(session[:profile_id]).kids and !@book.kids)
				@book = nil
			else
				@comentario = Commentary.new(profile_id: session[:profile_id], book_id: @book.id)
				@punto = Vote.new(profile_id: session[:profile_id], isbn: @book.isbn)

				#si ya existe buscarlo y guardarlo en @favorito
				if(current_user.profiles.find(session[:profile_id]).favorites.where(book_id:@book.id).size == 0)
					@favorite = Favorite.new(book_id: @book.id, profile_id: session[:profile_id])
				else
					@favorite = current_user.profiles.find(session[:profile_id]).favorites.where(book_id:@book.id).first
				end
				if(current_user.profiles.find(session[:profile_id]).seelaters.where(book_id:@book.id).size == 0)
					@seelater = Seelater.new(book_id: @book.id, profile_id: session[:profile_id])
				else
					@seelater = current_user.profiles.find(session[:profile_id]).seelaters.where(book_id:@book.id).first
				end
			end
		end
	end

	def edit
		redirect_to root_path unless current_user.admin?

		@book = Book.find params[:id]
		@autor = Author.new

	end

	def destroy
		redirect_to root_path unless current_user.admin?

		@book = Book.find params[:id]
		@book.destroy
		redirect_to books_path
	end

	def update
		redirect_to root_path unless current_user.admin?

		@book = Book.find params[:id]
		if(@book.update(params.require(:book).permit!))
			redirect_to books_indexadmin_path
			flash[:notice] = "Cambios realizados con éxito"
		else
			flash.now[:alert] = @book.errors.full_messages.first

			render :edit
		end
	end

	private

	def book_params
		params.require(:book).permit(:total,:punctuation,:scores,:titulo,:genre_id,:category_id,:kids,:resumen,:indate,:outdate,:picture,:isbn,:editorial,author_ids:[])
	end


end
