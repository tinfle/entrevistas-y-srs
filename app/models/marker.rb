class Marker < ApplicationRecord
  belongs_to :capitulo
  belongs_to :profile
  belongs_to :book
end
