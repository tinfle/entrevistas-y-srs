class Book < ApplicationRecord

   validate :indate_bigger_tan_outdate
   has_and_belongs_to_many :authors
   has_one_attached :image
   has_many :favorites
   has_many :seelaters
   has_many :markers
   belongs_to :genre
   belongs_to :category
   validates_uniqueness_of :isbn, :on => :create
   validates_uniqueness_of :isbn, :on => :update

   has_many :capitulos
   has_many :commentaries



  def indate_bigger_tan_outdate
    if indate > outdate
      errors.add(:indate, "La fecha de lanzamiento no puede ser mas lejana que la de vencimiento ")
    end
  end

end
