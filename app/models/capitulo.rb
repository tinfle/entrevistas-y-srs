class Capitulo < ApplicationRecord
	belongs_to :book
	validate :expiration_date_cannot_be_in_the_past
	validate :indate_bigger_tan_outdate
	has_many :markers
	
	def expiration_date_cannot_be_in_the_past
		if outdate.present? && outdate < Time.now
			errors.add(:outdate, "La fecha de vencimiento no puede estar en el pasado ")
		end
	end


	def indate_bigger_tan_outdate
		if indate > outdate
			errors.add(:indate, "La fecha de lanzamiento no puede ser mas lejana que la de vencimiento ")
		end
	end
end
