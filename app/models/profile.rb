class Profile < ApplicationRecord
  belongs_to :user
  validates_uniqueness_of :name, :on => :update
  has_many :votes
  has_many :favorites
  has_many :markers
  has_many :seelaters

end
