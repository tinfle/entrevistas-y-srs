# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


Genre.find_or_create_by(nombre:"Narrativo")
Genre.find_or_create_by(nombre:"Lírico")
Genre.find_or_create_by(nombre:"Dramático")


Category.find_or_create_by(nombre:"Ciencia")
Category.find_or_create_by(nombre:"Cine")
Category.find_or_create_by(nombre:"Deportes")
Category.find_or_create_by(nombre:"Historia")
Category.find_or_create_by(nombre:"Humor")
Category.find_or_create_by(nombre:"Infantiles")
Category.find_or_create_by(nombre:"Ficcion")
Category.find_or_create_by(nombre:"No Ficcion")
Category.find_or_create_by(nombre:"Transhumanismo")
Category.find_or_create_by(nombre:"Musica")
Category.find_or_create_by(nombre:"Arquitectura")
Category.find_or_create_by(nombre:"Politica")
Category.find_or_create_by(nombre:"Religion")
Category.find_or_create_by(nombre:"Adultos")
Category.find_or_create_by(nombre:"Viajes")
Category.find_or_create_by(nombre:"Tauromaquia")
Category.find_or_create_by(nombre:"Feminismo")
Category.find_or_create_by(nombre:"Otro")



Author.find_or_create_by(autor:"Gabriel Garcia Marquez")
Author.find_or_create_by(autor:"Pablo Neruda")
Author.find_or_create_by(autor:"Mario Vargas Llosa")
Author.find_or_create_by(autor:"Juan Carlos Onetti")
Author.find_or_create_by(autor:"Jose Marti")
Author.find_or_create_by(autor:"Jorge Luis Borges")
Author.find_or_create_by(autor:"Julio Cortazar")
Author.find_or_create_by(autor:"Isabel Allende")
Author.find_or_create_by(autor:"Jorge Armado")
Author.find_or_create_by(autor:"Miguel Asturias")
Author.find_or_create_by(autor:"Carlos Fuentes")
Author.find_or_create_by(autor:"Gabriela Mistral")
Author.find_or_create_by(autor:"Octavio Paz")
Author.find_or_create_by(autor:"Roberto Bolaño")
Author.find_or_create_by(autor:"Mario Benedetti")



