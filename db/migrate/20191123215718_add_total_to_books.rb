class AddTotalToBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :total, :integer
  end
end
