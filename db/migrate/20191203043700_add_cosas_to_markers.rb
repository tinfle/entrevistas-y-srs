class AddCosasToMarkers < ActiveRecord::Migration[5.2]
  def change
    add_reference :markers, :capitulo, index: true
    add_reference :markers, :profile, index: true
  end
end
