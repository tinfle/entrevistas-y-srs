class AddBookModelColumns < ActiveRecord::Migration[5.1]
  def change
    add_column :books, :titulo, :string
    add_column :books, :resumen, :text
    add_column :books, :autor, :string
    add_column :books, :isbn, :bigint
  end
end
