class AddAlgoToMarker < ActiveRecord::Migration[5.2]
  def change
      add_reference :markers, :book, index: true
  end
end
