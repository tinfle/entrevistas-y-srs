class AddEditorialToBook < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :editorial, :string
  end
end
