class AddDateAndAdressToUsers < ActiveRecord::Migration[5.1]
  def change
  	add_column :users, :name, :string
  	add_column :users, :surname, :string
    add_column :users, :date_of_birth, :datetime
    add_column :users, :adress, :string
  end
end
