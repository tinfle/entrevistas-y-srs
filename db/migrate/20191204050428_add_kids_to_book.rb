class AddKidsToBook < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :kids, :boolean
  end
end
