class AddPunctuationToBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :punctuation, :integer
  end
end
