class AddProfileToVotes < ActiveRecord::Migration[5.2]
  def change
    add_column :votes, :profile_id, :integer
  end
end
