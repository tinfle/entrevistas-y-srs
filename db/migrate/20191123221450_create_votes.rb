class CreateVotes < ActiveRecord::Migration[5.2]
  def change
    create_table :votes do |t|
      t.string :isbn
      t.integer :punctuation

      t.timestamps
    end
  end
end
