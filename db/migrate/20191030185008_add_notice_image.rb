class AddNoticeImage < ActiveRecord::Migration[5.1]
  def up
    add_attachment :notices, :image
  end

  def down
    remove_attachment :notices, :image
  end
end
