class AddCosasTofAvorites < ActiveRecord::Migration[5.2]
  def change
    add_reference :favorites, :user, index: true
    add_reference :favorites, :book, index: true
    add_reference :favorites, :profile, index: true
  end
end
