class CreateCommentaries < ActiveRecord::Migration[5.2]
  def change
    create_table :commentaries do |t|
      t.integer :profile_id
      t.integer :book_id
      t.string :comment

      t.timestamps
    end
  end
end
