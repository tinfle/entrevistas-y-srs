class AddColumnsToNotices < ActiveRecord::Migration[5.2]
  def change
    add_column :notices, :titulo, :string
    add_column :notices, :cuerpo, :text
  end
end
