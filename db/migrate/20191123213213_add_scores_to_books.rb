class AddScoresToBooks < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :scores, :integer
  end
end
