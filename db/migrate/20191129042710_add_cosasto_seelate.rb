class AddCosastoSeelate < ActiveRecord::Migration[5.2]
  def change
    add_reference :seelaters, :book, index: true
    add_reference :seelaters, :profile, index: true
  end
end
