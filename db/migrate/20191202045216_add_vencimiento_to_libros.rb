class AddVencimientoToLibros < ActiveRecord::Migration[5.2]
  def change
    add_column :books, :indate, :datetime
    add_column :books, :outdate, :datetime
    add_column :capitulos, :indate, :datetime
    add_column :capitulos, :outdate, :datetime
  end
end
