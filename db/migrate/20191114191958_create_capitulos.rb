class CreateCapitulos < ActiveRecord::Migration[5.2]
  def change
    create_table :capitulos do |t|

      t.string :titulo
      t.string :pdf
      t.references :book, index: true

      t.timestamps
    end
  end
end