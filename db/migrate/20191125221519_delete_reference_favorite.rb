class DeleteReferenceFavorite < ActiveRecord::Migration[5.2]
  def change
    remove_reference :favorites, :user, index: true
  end
end
