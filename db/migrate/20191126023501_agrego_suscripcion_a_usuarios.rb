class AgregoSuscripcionAUsuarios < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :card, :bigint
    add_column :users, :cvv, :int
    add_column :users, :vencimiento, :datetime
  end
end
